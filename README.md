# DrupalOps: Servers

DrupalOps is a Family of Tools & Community that leverages Drupal & Symfony for IT Infrastructure Management.

See https://www.drupal.org/project/drupalops for more information.

## Architecture

This module just provides the base "server" node type. 

Similar to `hosting_server.module`, but with all of the IP Address handing and Aegir Hosting Services code ripped out.

Those tools will go into other modules.