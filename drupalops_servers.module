<?php

/**
 * @file
 * Main DrupalOps Servers module file.
 */

/**
 * This server has been deleted.
 */
define('DRUPALOPS_SERVER_DELETED', -2);

/**
 * This server has been locked. Its services will not be available for use.
 */
define('DRUPALOPS_SERVER_LOCKED', -1);

/**
 * This server has been queued for verification.
 */
define('DRUPALOPS_SERVER_QUEUED', 0);

/**
 * This server is created and enabled. Its services will be available for use.
 */
define('DRUPALOPS_SERVER_ENABLED', 1);

/**
 * Helper function to map status codes to labels and classes.
 */
function _drupalops_servers_status_codes() {
  $codes = array(
    DRUPALOPS_SERVER_QUEUED => array(
      'label' => 'Queued',
      'class' => 'hosting-queue',
    ),
    DRUPALOPS_SERVER_ENABLED => array(
      'label' => 'Enabled',
      'class' => 'hosting-success',
    ),
    DRUPALOPS_SERVER_DELETED => array(
      'label' => 'Deleted',
      'class' => 'hosting-error',
    ),
    DRUPALOPS_SERVER_LOCKED => array(
      'label' => 'Locked',
      'class' => 'hosting-disable',
    ),
  );
  return $codes;
}

/**
 * Server status codes to human-readable label map.
 */
function _drupalops_servers_status_codes_labels() {
  $labels = array();
  foreach (_drupalops_servers_status_codes() as $code => $info) {
    $labels[$code] = $info['label'];
  }
  return $labels;
}

/**
 * Return the appropriate status label.
 */
function _drupalops_servers_status($status) {
  static $labels;
  $labels = _drupalops_servers_status_codes();
  return is_object($status) ? $labels[$status->server_status]['label'] : $labels[$status]['label'];
}

/**
 * Define the classes that correspond to the platform status.
 */
function _drupalops_servers_list_class($status) {
  static $labels;
  $labels = _drupalops_servers_status_codes();
  return is_object($status) ? $labels[$status->server_status]['class'] : $labels[$status]['class'];
}

/**
 * Implements hook_node_info().
 */
function drupalops_servers_node_info() {
  $types["server"] = array(
    "type" => 'server',
    "name" => t('Server'),
    'base' => 'drupalops_servers',
    "has_title" => TRUE,
    "title_label" => t('Hostname'),
    "description" => t("A machine which will provide various services to your sites."),
    "has_body" => 0,
    "body_label" => '',
    "min_word_count" => 0,
  );
  return $types;
}

/**
 * Implements hook_menu().
 */
function drupalops_servers_menu() {
  $items = array();

  $items['servers/list'] = array(
    'title' => 'List',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );

  $items['servers/add'] = array(
    'title' => 'Add server',
    'type' => MENU_LOCAL_TASK,
    'page callback' => 'drupal_goto',
    'page arguments' => array('node/add/server'),
    'access callback' => 'node_access',
    'access arguments' => array('create', 'server'),
  );

  return $items;
}

/**
 * Hide the delete button on server nodes.
 */
function drupalops_servers_form_alter(&$form, &$form_state, $form_id) {
  // Remove delete button from server edit form,
  // unless the server's already been deleted via the Delete task.
  // @TODO: Create a clear way to allow Super Admins to delete the node. (Like a DANGER ZONE).
  if ($form_id == 'server_node_form') {
    $node = $form['#node'];
    if (isset($node->server_status) && $node->server_status !== DRUPALOPS_SERVER_DELETED) {
      $form['actions']['delete']['#type'] = 'hidden';
    }
  }
}

/**
 * Implements hook_form().
 */
function drupalops_servers_form($node, &$form_state) {
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Server hostname'),
    '#required' => TRUE,
    '#default_value' => $node->title,
    '#description' => t('The host name of the server. This is the address that will be used by Hostmaster to connect to the server to issue commands. It is to resolve to the internal network, if you have such a separation.<br /><strong>Be careful when changing the node title, it is used to create the SQL users if the IP address, below, is not set.</strong>'),
    '#weight' => 0,
  );

  $form['human_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Human-readable name'),
    '#default_value' => isset($node->human_name) ? $node->human_name : '',
    '#description' => t('<strong>Optional, but recommended.</strong> Internally, Aegir refers to servers by their hostname. The value of this field will be used throughout the UI, and allows for more meaningful names. While not enforced by the system, it is highly recommended that the name assigned to a server be unique.'),
    '#weight' => 1,
  );

  return $form;
}

/**
 * Implements hook_validate().
 */
function drupalops_servers_validate(&$node, &$form, &$form_state) {
  if ($node->op != t('Delete')) {
    // Make sure the server name is unique, to avoid context clashes.
    $result = db_query("SELECT n.title AS name
                          FROM {drupalops_servers} AS h
                    INNER JOIN {node} AS n ON n.nid = h.nid
                         WHERE n.title = :title
                           AND n.nid <> :nid
                           AND h.status >= :status",
      array(
        ':title' => $node->title,
        ':nid' => empty($node->nid) ? '' : $node->nid,
        ':status' => DRUPALOPS_SERVER_LOCKED,
      )
    )->fetchField();
    if ($result) {
      form_set_error('title',
        t('The hostname `%name` is already in use. Server names must be unique.',
        array('%name' => $result))
      );
    }

    if (!_hosting_valid_fqdn($node->title)) {
      form_set_error('title', t('Invalid hostname. Use a proper hostname or an IP address.'));
    }
  }
}

/**
 * Implements hook_update().
 */
function drupalops_servers_update($node) {
  // If this is a new node or we're adding a new revision.
  if (!empty($node->revision)) {
    drupalops_servers_insert($node);
  }

  if ($node->server_status == DRUPALOPS_SERVER_DELETED) {
    $node->no_verify = TRUE;
  }

  $id = db_update('drupalops_servers')
    ->fields(array(
      'human_name' => isset($node->human_name) ? $node->human_name : '',
      'verified' => isset($node->verified) ? $node->verified : 0,
      'status' => isset($node->server_status) ? $node->server_status : 0,
    ))
    ->condition('nid', $node->nid)
    ->execute();
}

/**
 * Implements hook_insert().
 */
function drupalops_servers_insert($node) {
  $id = db_insert('drupalops_servers')
    ->fields(array(
      'vid' => $node->vid,
      'nid' => $node->nid,
      'human_name' => isset($node->human_name) ? $node->human_name : '',
      'verified' => isset($node->verified) ? $node->verified : 0,
      'status' => isset($node->server_status) ? $node->server_status : 0,
    ))
    ->execute();
}

/**
 * Implements hook_delete_revision().
 */
function hosting_nodeapi_server_delete_revision(&$node) {
  db_delete('drupalops_servers')
    ->condition('vid', $node->vid)
    ->execute();
}

/**
 * Implements hook_delete().
 */
function drupalops_servers_delete($node) {
  db_delete('drupalops_servers')
    ->condition('nid', $node->nid)
    ->execute();
}

/**
 * Implements hook_load().
 */
function drupalops_servers_load($nodes) {
  foreach ($nodes as $nid => &$node) {
    $additions = db_query('SELECT human_name, verified, status AS server_status FROM {drupalops_servers} WHERE vid = :vid', array(':vid' => $node->vid))->fetch();
    // Avoid PHP 5.4 warning when platform doesn't exist yet.
    // See: https://drupal.org/node/1940378
    $additions = $additions ? $additions : new stdClass();
    foreach ($additions as $property => &$value) {
      $node->$property = is_numeric($value) ? (int) $value : $value;
    }
  }
}

/**
 * Implements hook_view().
 */
function drupalops_servers_view($node, $view_mode, $langcode = NULL) {

  $node->content['info'] = array(
    '#type' => 'fieldset>',
  );
  $node->content['info']['status'] = array(
    '#type' => 'item',
    '#title' => t('Status'),
    '#markup' => _drupalops_servers_status($node->server_status),
  );
  $node->content['info']['verified'] = array(
    '#type' => 'item',
    '#title' => t('Verified'),
    '#markup' => drupalops_format_interval($node->verified),
    '#weight' => -10,
  );
  $node->content['info']['hostname'] = array(
    '#title' => 'hostname',
    '#type' => 'item',
    '#markup' => $node->title,
    '#weight' => -50,
  );

  if ($node->human_name) {
    $node->title = $node->human_name . "<small>$node->title</small>";
  }

  return $node;
}

/**
 * Implements hook_field_extra_fields().
 */
function drupalops_servers_field_extra_fields() {
  $return['node']['server'] = array(
    'display' => array(
      'info' => array(
        'label' => t('Aegir Server Information'),
        'description' => t('Detailed information about this server.'),
        'weight' => 0,
      ),
    ),
  );
  return $return;
}

/**
 * Implements hook_entity_property_info().
 */
function drupalops_servers_entity_property_info() {
  $info['node']['bundles']['server']['properties'] = array(
    'human_name' => array(
      'label' => t('Human-readable name'),
      'description' => t('The optional human readable name of the server.'),
      'type' => 'text',
    ),
    'verified' => array(
      'label' => t('Last verification time'),
      'description' => t('The date and time of the last verification of the server.'),
      'type' => 'date',
    ),
    'server_status' => array(
      'label' => t('Server status'),
      'description' => t('The status of the server. E.g. enabled, deleted, etc.'),
      'type' => 'integer',
      'options list' => '_drupalops_servers_status_codes_labels',
    ),
  );

  return $info;
}

/**
 * Views integration.
 * @TODO
 */
function drupalops_servers_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'drupalops_servers') . '/includes/views',
  );
}

/**
 * Check if the FQDN provided is valid.
 *
 * @param string $fqdn
 *   The Fully Qualified Domain Name (FQDN) to validate.
 *
 * @return int
 *   An integer greater than 0 if the $fqdn is valid, or 0 or FALSE if it not
 *   valid.
 */
function _hosting_valid_fqdn($fqdn) {
  // Regex is an implementation of RFC1035, a little relaxed to allow
  // commonly registered hostnames (e.g. domaines starting with digits).
  return preg_match("/^([a-z0-9]([a-z0-9-]*[a-z0-9])?\.?)+$/i", $fqdn);
}

/**
 * Format a timestamp as a string in a friendly way.
 *
 * @param int $ts
 *   The timestamp to format as a an interval.
 *
 * @return string
 *   Returns a string representing the given timestamp:
 *   - If the timestamp is the current time: 'Now'.
 *   - If the timestamp is 0 or FALSE: 'Never'
 *   - Otherwise formatted as 'X ago' where 'X' is for example 1 year or 1
 *     minute etc.
 *
 * @see format_interval()
 */
function drupalops_format_interval($ts) {
  if ($ts == REQUEST_TIME) {
    return t('Now');
  }
  if ($ts <= 1) {
    // Treats the UNIX EPOCH as never.
    return t('Never');
  }

  return t("@interval ago", array("@interval" => format_interval(REQUEST_TIME - $ts, 1)));
}
