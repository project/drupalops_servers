<?php

/**
 * A handler for the Server status field.
 *
 * @ingroup views_field_handlers
 */
class drupalops_servers_handler_field_status extends views_handler_field {

  /**
   * Constructor; calls to base object constructor.
   */
  function construct() {
    parent::construct();
    $this->additional_fields = array('verified');
  }

  /**
   * Define options: display mode.
   * @return mixed
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['status_mode'] = array('default' => 'text');

    return $options;
  }

  /**
   * Form for status display mode.
   * @param $form
   * @param $form_state
   */
  function options_form(&$form, &$form_state) {
    $form['status_mode'] = array(
      '#type' => 'radios',
      '#title' => t('Display mode'),
      '#options' => array(
        'text' => t('Text'),
        'image' => t('Image'),
        'text_image' => t('Text & Image'),
        'class' => t('CSS class'),
      ),
      '#default_value' => isset($this->options['status_mode']) ? $this->options['status_mode'] : 'text',

      '#description' => t("Display mode of status values.")
    );
    parent::options_form($form, $form_state);
  }

  /**
   * Override the display by adding classes for icons.
   *
   * @param $values
   * @return mixed|string
   */
  function render($values) {
    $value = $values->{$this->field_alias};
    $output = _drupalops_servers_status($value);

    switch ($this->options['status_mode']) {
      case 'image':
        return "<span class='hosting-status hosting-status-icon'></span>";

      case 'text_image':
        return "<span class='hosting-status'>{$output}</span>";

      case 'class':
        return _drupalops_servers_list_class($value);
    }
    return $output;
  }

}
