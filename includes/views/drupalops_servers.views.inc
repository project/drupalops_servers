<?php
/**
 * @file
 * DrupalOps Servers views integration.
 */

/**
 * Implements hook_views_data().
 */
function drupalops_servers_views_data() {
  $data['drupalops_servers']['table'] = array(
    'group' => 'Hosting Server',
    'title' => 'Server',
    'join' => array(
      'node' => array(
        'left_field' => 'vid',
        'field' => 'vid',
      ),
    ),
  );

  $data['drupalops_servers']['human_name'] = array(
    'title' => t('Human-readable name'),
    'help' => t('A human-readable name for the server.'),
    'field' => array(
      'handler' => 'drupalops_servers_handler_field_human_name',
      'click sortable' => TRUE,
    ),
  );

  $data['drupalops_servers']['verified'] = array(
    'title' => t('Verified Date'),
    'help' => t('The most recent date that this server was verified.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
  );

  $data['drupalops_servers']['status'] = array(
    'title' => t('Status'),
    'help' => t('The current state of this server.'),
    'field' => array(
      'handler' => 'drupalops_servers_handler_field_status',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );

  return $data;
}
